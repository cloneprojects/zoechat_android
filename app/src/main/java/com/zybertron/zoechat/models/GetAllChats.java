package com.zybertron.zoechat.models;

import org.json.JSONObject;

import java.util.List;

public class GetAllChats {
    List<JSONObject> normalChatList;

    public List<JSONObject> getNormalChatList() {
        return normalChatList;
    }

    public void setNormalChatList(List<JSONObject> normalChatList) {
        this.normalChatList = normalChatList;
    }

    public List<JSONObject> getPinnedChatList() {
        return pinnedChatList;
    }

    public void setPinnedChatList(List<JSONObject> pinnedChatList) {
        this.pinnedChatList = pinnedChatList;
    }

    public GetAllChats(List<JSONObject> normalChatList, List<JSONObject> pinnedChatList) {

        this.normalChatList = normalChatList;
        this.pinnedChatList = pinnedChatList;
    }

    public GetAllChats() {

    }

    List<JSONObject> pinnedChatList;
}