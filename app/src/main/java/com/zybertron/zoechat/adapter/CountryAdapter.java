package com.zybertron.zoechat.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.zybertron.zoechat.R;
import com.zybertron.zoechat.baseUtils.SharedHelper;

import org.json.JSONArray;

/**
 * Created by admin on 18-07-2018.
 */

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {
    String sPosition;
    private JSONArray list;
    private Context myContext;


    public CountryAdapter(Context activity, JSONArray jsonArray, String position) {
        this.list = jsonArray;
        this.myContext = activity;
        this.sPosition = position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(myContext).inflate(R.layout.country_lists, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.country_name.setText(list.optJSONObject(position).optString("name"));
        holder.country_code.setText(list.optJSONObject(position).optString("phone-code"));
        String selectedcode = SharedHelper.getKey(myContext, "selected_code");
        if (selectedcode.equalsIgnoreCase(list.optJSONObject(position).optString("phone-code"))) {
            holder.isSelected.setVisibility(View.VISIBLE);
            holder.country_name.setTextColor(myContext.getResources().getColor(R.color.themegreen));
            holder.country_code.setTextColor(myContext.getResources().getColor(R.color.themegreen));
        } else {
            holder.country_name.setTextColor(myContext.getResources().getColor(R.color.black));
            holder.country_code.setTextColor(myContext.getResources().getColor(R.color.black));

            holder.isSelected.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedHelper.putKey(myContext, "selected_name", list.optJSONObject(position).optString("name"));
                SharedHelper.putKey(myContext, "selected_code", list.optJSONObject(position).optString("phone-code"));
                Intent returnIntent = new Intent();
                ((Activity) myContext).setResult(Activity.RESULT_OK, returnIntent);
                ((Activity) myContext).finish();

            }
        });

    }


    @Override
    public int getItemCount() {
        return list.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView isSelected;
        TextView country_name, country_code;

        public ViewHolder(View itemView) {
            super(itemView);

            country_name = (TextView) itemView.findViewById(R.id.country_name);
            country_code = (TextView) itemView.findViewById(R.id.country_code);
            isSelected = (ImageView) itemView.findViewById(R.id.selected);
        }
    }
}
