package com.zybertron.zoechat.activity;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SearchView;

import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;
import com.zybertron.zoechat.R;
import com.zybertron.zoechat.adapter.CountryAdapter;
import com.zybertron.zoechat.baseUtils.ReadFiles;
import com.zybertron.zoechat.baseUtils.SharedHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CountryCodeActivity extends AppCompatActivity implements android.support.v7.widget.SearchView.OnQueryTextListener {

    RecyclerView country_lists;
    CountryAdapter countryAdapter;
    String sPosition = " ";
    private String TAG = CountryCodeActivity.class.getSimpleName();
    private Toolbar toolbar;
    private ArrayList<String> phone_code;
    private LinearLayoutManager linearLayoutManager;
    private String selected_name, selected_code;
    JSONArray complete_lists = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_code);
        selected_name = SharedHelper.getKey(CountryCodeActivity.this, "selected_name");
        selected_code = SharedHelper.getKey(CountryCodeActivity.this, "selected_code");
        complete_lists = parseCountryCodes();
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });*/
//        toolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_arrow_back_theme));

        country_lists = (RecyclerView) findViewById(R.id.country_lists);
        country_lists.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
                .color(Color.parseColor("#dfdfdf"))
                .sizeResId(R.dimen.divider)
                .marginResId(R.dimen.leftmargin_star, R.dimen.rightmargin_star)
                .build());
        linearLayoutManager = new LinearLayoutManager(CountryCodeActivity.this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setAutoMeasureEnabled(true);


        countryAdapter = new CountryAdapter(CountryCodeActivity.this, parseCountryCodes(), sPosition);
        country_lists.setLayoutManager(linearLayoutManager);
        country_lists.setHasFixedSize(false);
        country_lists.setAdapter(countryAdapter);

    }

    private JSONArray getFilteredArray(CharSequence s) {
        String check = String.valueOf(s);
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < complete_lists.length(); i++) {
            String name = complete_lists.optJSONObject(i).optString("name");
            if (name.toLowerCase().contains(check.toLowerCase())) {
                jsonArray.put(complete_lists.optJSONObject(i));
            }
        }
        return jsonArray;
    }

    public JSONArray parseCountryCodes() {
        String response = "";
        ArrayList<String> list = new ArrayList<String>();
        phone_code = new ArrayList<String>();
        JSONArray array = new JSONArray();
        try {
            response = ReadFiles.readRawFileAsString(this,
                    R.raw.countrycodes);

            array = new JSONArray(response);


            for (int i = 0; i < array.length(); i++) {

                if (selected_name.equalsIgnoreCase(array.optJSONObject(i).optString("name"))) {
                    if (i > 10) {
                        int index = i;
                        JSONObject jsonObject = new JSONObject();
                        jsonObject = array.optJSONObject(i);
                        array.remove(index);
                        array.put(0, jsonObject);
                    }
                }


            }


        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        toolbar.getMenu().clear();
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.only_search, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        android.support.v7.widget.SearchView view = (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(searchItem);
        view.setIconifiedByDefault(false);
        view.setOnQueryTextListener(this);
        EditText searchEditText = (EditText) view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setHintTextColor(getResources().getColor(R.color.themegreen));
        searchEditText.setTextColor(getResources().getColor(R.color.white));
//        searchItem.expandActionView();
        ImageView searchViewIcon = (ImageView) view.findViewById(android.support.v7.appcompat.R.id.search_mag_icon);

        ViewGroup linearLayoutSearchView = (ViewGroup) searchViewIcon.getParent();
        linearLayoutSearchView.removeView(searchViewIcon);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        android.support.v7.widget.SearchView searchView = null;
        if (searchItem != null) {
            searchView = (android.support.v7.widget.SearchView) searchItem.getActionView();

            searchView.setQueryHint(Html.fromHtml("<font color = #FFFFFF>" + getResources().getString(R.string.search) + "</font>"));
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
//                if (chatsList.size() > 0)
//
//                    chatsAdapter.setFilter(chatsList);

                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() > 1) {

            country_lists.setLayoutManager(new LinearLayoutManager(CountryCodeActivity.this));
            countryAdapter = new CountryAdapter(CountryCodeActivity.this, getFilteredArray(newText), "");
            country_lists.setAdapter(countryAdapter);
            countryAdapter.notifyDataSetChanged();

        }
        return true;
    }
}
