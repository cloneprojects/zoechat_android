package com.zybertron.zoechat.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.api.client.json.Json;
import com.squareup.picasso.Picasso;
import com.zybertron.zoechat.DBHelper.DBHandler;
import com.zybertron.zoechat.R;
import com.zybertron.zoechat.baseUtils.AsyncTaskCompleteListener;
import com.zybertron.zoechat.baseUtils.SharedHelper;
import com.zybertron.zoechat.models.GetUserFromDBModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ContactActivity extends AppCompatActivity implements AsyncTaskCompleteListener, SearchView.OnQueryTextListener {

    private static final String TAG = ContactActivity.class.getSimpleName();
    public static List<GetUserFromDBModel> list_of_contacts = new ArrayList<>();
    public static List<GetUserFromDBModel> s_chatslist = new ArrayList<>();
    RecyclerView recyclerView;
    DBHandler dbHandler = new DBHandler(ContactActivity.this);
    private List<JSONObject> participantsList = new ArrayList<>();
    private RelativeLayout errorLayout;
    private SelectContact selectContact;
    private Toolbar toolbar;
    private JSONArray jsonArray;

    public static int getPrimaryCOlor(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
        return value.data;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public static int getPrimaryDark(Context context) {
        final TypedValue value = new TypedValue();
        context.getTheme().resolveAttribute(R.attr.colorPrimaryDark, value, true);
        return value.data;
    }

    public static void customView(View v, int backgroundColor, int borderColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[]{100, 100, 100, 100, 100, 100, 100, 100});
        shape.setColor(backgroundColor);
        shape.setStroke(3, borderColor);
        v.setBackgroundDrawable(shape);
    }

    private void Setheme(String themevalue) {
        switch (themevalue) {
            case "1":
                setTheme(R.style.AppThemeGreen);
                break;
            case "2":
                setTheme(R.style.AppThemeBlue);
                break;
            case "3":
                setTheme(R.style.AppThemeIndigo);
                break;
            case "4":
                setTheme(R.style.AppThemeGrey);
                break;
            case "5":
                setTheme(R.style.AppThemeYellow);
                break;
            case "6":
                setTheme(R.style.AppThemeOrange);
                break;
            case "7":
                setTheme(R.style.AppThemePurple);
                break;
            case "8":
                setTheme(R.style.AppThemePaleGreen);
                break;
            case "9":
                setTheme(R.style.AppThemelightBlue);
                break;
            case "10":
                setTheme(R.style.AppThemePink);
                break;
            case "11":
                setTheme(R.style.AppThemelightGreen);
                break;
            case "12":
                setTheme(R.style.AppThemelightRed);
                break;
            default:
                setTheme(R.style.AppThemeGreen);
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_contact);

        toolbar = (Toolbar) findViewById(R.id.contact_toolbar);
        toolbar.setTitle("Contacts to send");
        toolbar.setSubtitle("0 Selected");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        toolbar.setBackgroundColor(getPrimaryCOlor(ContactActivity.this));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setSupportActionBar(toolbar);


        errorLayout = (RelativeLayout) findViewById(R.id.error_layout);
        recyclerView = (RecyclerView) findViewById(R.id.contact_list);
        recyclerView.setHasFixedSize(true);

        //list_of_contacts = dbHandler.GetAllUserFromDB();
        jsonArray = dbHandler.GetAllUserFromDBArray();
        Log.e(TAG, "jsonArray: " + jsonArray);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ContactActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

      /*  selectContact = new SelectContact(ContactActivity.this, jsonArray, toolbar);
        recyclerView.setAdapter(selectContact);*/

        if (jsonArray.length() > 0) {
            /*Collections.sort(jsonArray, new Comparator<GetUserFromDBModel>() {
                @Override
                public int compare(GetUserFromDBModel getUserFromDB, GetUserFromDBModel t1) {
                    return getUserFromDB.getName().toLowerCase().compareTo(t1.getName().toLowerCase());
                }
            });*/
            selectContact = new SelectContact(ContactActivity.this, jsonArray, toolbar);
            recyclerView.setAdapter(selectContact);
        } else {
            errorLayout.setVisibility(View.VISIBLE);
        }
        ImageView send_contact = (ImageView) findViewById(R.id.send_contact);
        customView(send_contact, getPrimaryCOlor(ContactActivity.this), getPrimaryCOlor(ContactActivity.this));

        send_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (participantsList.size() != 0) {
                        Intent intent = new Intent();
                        intent.putExtra("selected_contact", participantsList.toString());
                        setResult(103, intent);
                        finish();
                    } else {
                        Toast.makeText(ContactActivity.this, getResources().getString(R.string.select_one), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {

                }


            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_only, menu);
        MenuItem menuItem = menu.findItem(R.id.contact_search);
        try {
            SearchView view = (SearchView) MenuItemCompat.getActionView(menuItem);
            EditText searchEditText = (EditText) view.findViewById(android.support.v7.appcompat.R.id.search_src_text);
            searchEditText.setHintTextColor(getResources().getColor(R.color.white));
            searchEditText.setTextColor(getResources().getColor(R.color.white));

            view.setOnQueryTextListener(this);
            MenuItemCompat.setOnActionExpandListener(menuItem, new MenuItemCompat.OnActionExpandListener() {
                @Override
                public boolean onMenuItemActionExpand(MenuItem item) {
                    return true;
                }

                @Override
                public boolean onMenuItemActionCollapse(MenuItem item) {
                    return true;
                }
            });

        } catch (Exception e) {

        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onTaskCompleted(JSONObject response, int serviceCode) {
        Log.e("res", response.toString());
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //  List<GetUserFromDBModel> getUserFromDBs = filter(list_of_contacts, newText);
        selectContact.getFilter().filter(newText);
        return true;

    }

    private List<GetUserFromDBModel> filter(List<GetUserFromDBModel> list, String query) {
        query = query.toLowerCase();
        List<GetUserFromDBModel> filter_list = new ArrayList<>();
        for (GetUserFromDBModel getUserFromDB : list) {
            String type_value = getUserFromDB.getName().toLowerCase();
            if (type_value.contains(query)) {
                filter_list.add(getUserFromDB);
            }
        }
        return filter_list;

    }


    private class SelectContact extends RecyclerView.Adapter<SelectContact.MyViewHolder> implements Filterable {
        private List<GetUserFromDBModel> list;
        private JSONArray jsonArray;
        private Context mContext;
        private Toolbar sub;
        private CustomFilter customFilter;

        SelectContact(ContactActivity contactActivity, List<GetUserFromDBModel> list_of_contacts, Toolbar toolbar) {
            this.list = list_of_contacts;
            this.mContext = contactActivity;
            this.sub = toolbar;
        }

        SelectContact(ContactActivity contactActivity, JSONArray jsonArray, Toolbar toolbar) {
            this.jsonArray = jsonArray;
            this.mContext = contactActivity;
            this.sub = toolbar;
            customFilter = new CustomFilter(SelectContact.this, this.jsonArray);
        }

        public class CustomFilter extends Filter {
            private SelectContact mAdapter;
            private JSONArray mJsonArray;

            private CustomFilter(SelectContact mAdapter, JSONArray jsonArray) {
                super();
                this.mAdapter = mAdapter;
                this.mJsonArray = jsonArray;
            }

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                JSONArray jsonArrays = new JSONArray();
                Log.e("dddd", "charString: " + charSequence);
                if (charString.isEmpty()) {
                    Log.e("dddd", "empty: ");
                    jsonArrays = mJsonArray;
                } else {

                    Log.e("dddd", "not empty: ");
                    JSONArray filterArray = new JSONArray();
                    Log.e("dddd", "jsonArray: " + mJsonArray);
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        if (mJsonArray.optJSONObject(i).optString("Name")
                                .toLowerCase().contains(charString.toLowerCase())) {
                            Log.e("dddd", "matches: ");

                            filterArray.put(mJsonArray.optJSONObject(i));
                        }
                    }

                    jsonArrays = filterArray;

                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = jsonArrays;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults filterResults) {
                jsonArray = (JSONArray) filterResults.values;
                mAdapter.notifyDataSetChanged();
            }
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.new_contact_list_item, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            final JSONObject jsonObject = jsonArray.optJSONObject(holder.getAdapterPosition());

            if (jsonObject.optString("Image").equalsIgnoreCase("")) {
                Picasso.with(mContext).load(R.drawable.ic_account_circle)
                        .error(mContext.getResources().getDrawable(R.drawable.ic_account_circle)).into(holder.user_image);
            } else {
                Picasso.with(mContext).load(jsonObject.optString("Image")).into(holder.user_image);
            }
            holder.user_name.setText(jsonObject.optString("Name"));
            holder.user_status.setText(jsonObject.optString("Status"));

            if (jsonObject.optString("isSelected").equalsIgnoreCase("true")) {

                holder.click.setVisibility(View.VISIBLE);
                holder.show_view_check.setVisibility(View.GONE);
            } else {

                holder.click.setVisibility(View.GONE);
                holder.show_view_check.setVisibility(View.VISIBLE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (jsonObject.optString("isSelected").equalsIgnoreCase("true")) {

                        try {
                            jsonObject.put("isSelected", "false");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String clickId = jsonObject.optString("ZoeChatId");
                        if (participantsList.size() == 1) {
                            participantsList.clear();
                            sub.setSubtitle("0 Selected");
                        } else {

                            for (int i = 0; i < participantsList.size(); i++) {
                                String id = participantsList.get(i).optString("zoeChatId");
                                if (id.equalsIgnoreCase(clickId)) {
                                    participantsList.remove(i);
                                    sub.setSubtitle(String.valueOf(participantsList.size() + " Selected"));
                                    break;
                                }
                            }
                        }


                    } else {

                        try {
                            jsonObject.put("isSelected", "true");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        JSONObject jsonObject1 = new JSONObject();
                        JSONObject pat_Obj = new JSONObject();
                        try {
                            jsonObject1.put("name", jsonObject.optString("Name"));
                            jsonObject1.put("image", jsonObject.optString("Image"));
                            jsonObject1.put("zoeChatId", jsonObject.optString("ZoeChatId"));
                            jsonObject1.put("mobile", jsonObject.optString("Mobile"));
                            pat_Obj.put("participantId", jsonObject.optString("ZoeChatId"));
                            jsonObject1.put("status", jsonObject.optString("Status"));
                            participantsList.add(jsonObject1);

                            sub.setSubtitle(String.valueOf(participantsList.size() + " Selected"));
                            Log.d("addedList", "" + participantsList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public Filter getFilter() {
            return customFilter;
        }

        @Override
        public int getItemCount() {
            return jsonArray.length();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {
            CircleImageView user_image;
            TextView user_name, user_status;
            ImageView click, show_view_check;
            RelativeLayout showCheckLayout;

            MyViewHolder(View itemView) {
                super(itemView);
                user_image = (CircleImageView) itemView.findViewById(R.id.contact_image);
                user_name = (TextView) itemView.findViewById(R.id.user_name);
                showCheckLayout = (RelativeLayout) itemView.findViewById(R.id.showCheckLayout);
                user_status = (TextView) itemView.findViewById(R.id.user_status);
                click = (ImageView) itemView.findViewById(R.id.remove_view_check);
                show_view_check = (ImageView) itemView.findViewById(R.id.show_view_check);
                customView(click, getPrimaryCOlor(ContactActivity.this), getPrimaryCOlor(ContactActivity.this));

            }
        }
    }
}
