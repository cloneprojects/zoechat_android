package com.zybertron.zoechat.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.zybertron.zoechat.R;

public class test extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
//contact activity
/**/
// try {
//         if (newText.length() > 0) {
//
//         if (getUserFromDBs.size() > 0) {
//
//         if (jsonArray.length() > 0) {
////                        Collections.sort(list_of_contacts, new Comparator<GetUserFromDBModel>() {
////                            @Override
////                            public int compare(GetUserFromDBModel getUserFromDB, GetUserFromDBModel t1) {
////                                return getUserFromDB.getName().toLowerCase().compareTo(t1.getName().toLowerCase());
////                            }
////                        });
//
//         selectContact = new SelectContact(ContactActivity.this, getUserFromDBs, toolbar);
//         recyclerView.setAdapter(selectContact);
//         selectContact.notifyDataSetChanged();
//
//         } else {
//         errorLayout.setVisibility(View.VISIBLE);
//         }
//
//         } else {
//         // getUserFromDBs = new ArrayList<>();
//         jsonArray = new JSONArray();
////                    Collections.sort(getUserFromDBs, new Comparator<GetUserFromDBModel>() {
////                        @Override
////                        public int compare(GetUserFromDBModel getUserFromDB, GetUserFromDBModel t1) {
////                            return getUserFromDB.getName().toLowerCase().compareTo(t1.getName().toLowerCase());
////                        }
////                    });
//         selectContact = new SelectContact(ContactActivity.this, jsonArray, toolbar);
//         recyclerView.setAdapter(selectContact);
//         selectContact.notifyDataSetChanged();
//
//         }
//
//
//         } else {
//         //list_of_contacts = new ArrayList<>();
//         //list_of_contacts = dbHandler.GetAllUserFromDB();
//         jsonArray = new JSONArray();
//         jsonArray = dbHandler.GetAllUserFromDBArray();
//         recyclerView.setLayoutManager(new LinearLayoutManager(ContactActivity.this));
//
//                /*Collections.sort(list_of_contacts, new Comparator<GetUserFromDBModel>() {
//                    @Override
//                    public int compare(GetUserFromDBModel getUserFromDB, GetUserFromDBModel t1) {
//                        return getUserFromDB.getName().toLowerCase().compareTo(t1.getName().toLowerCase());
//                    }
//                });*/
//
//         selectContact = new SelectContact(ContactActivity.this, jsonArray, toolbar);
//         recyclerView.setAdapter(selectContact);
//         selectContact.notifyDataSetChanged();
//         }
//         } catch (Exception e) {
//         e.printStackTrace();
//         }
//         return true;
//contact acitivity adapter class
/* if (list.get(position).getImage().equalsIgnoreCase("")) {
                Picasso.with(mContext).load(R.drawable.ic_account_circle)
                        .error(mContext.getResources().getDrawable(R.drawable.ic_account_circle)).into(holder.user_image);
            } else {
                Picasso.with(mContext).load(list.get(position).getImage()).into(holder.user_image);
            }
            holder.user_name.setText(list.get(position).getName());
            holder.user_status.setText(list.get(position).getStatus());

            if (participantsList.size() > 0) {
                for (int i = 0; i < participantsList.size(); i++) {
                    String id = participantsList.get(i).optString("mobile");
                    String clickId = list.get(position).getMobile();
                    Log.d("onBindViewHolder: ", "id:" + id + ",click:" + clickId);
                    if (id.equalsIgnoreCase(clickId)) {
                        holder.click.setVisibility(View.VISIBLE);
                    } else {
                        holder.click.setVisibility(View.GONE);
                    }
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.click.getVisibility() == View.VISIBLE) {
                        String clickId = list.get(position).getZoeChatId();
                        if (participantsList.size() == 1) {
                            participantsList.clear();
                            sub.setSubtitle("0 Selected");
                        } else {
                            for (int i = 0; i < participantsList.size(); i++) {
                                String id = participantsList.get(i).optString("zoeChatId");
                                if (id.equalsIgnoreCase(clickId)) {
                                    participantsList.remove(i);
                                    sub.setSubtitle(String.valueOf(participantsList.size() + " Selected"));
                                    break;
                                }

                            }

                        }

                        holder.click.setVisibility(View.GONE);
                        Log.d("addedList", "" + participantsList);
                        // notifyDataSetChanged();

                    } else if (holder.click.getVisibility() == View.GONE) {

                        JSONObject jsonObject = new JSONObject();
                        JSONObject pat_Obj = new JSONObject();
                        try {
                            jsonObject.put("name", list.get(position).getName());
                            jsonObject.put("image", list.get(position).getImage());
                            jsonObject.put("zoeChatId", list.get(position).getZoeChatId());
                            jsonObject.put("mobile", list.get(position).getMobile());
                            pat_Obj.put("participantId", list.get(position).getZoeChatId());
                            jsonObject.put("status", list.get(position).getStatus());
                            participantsList.add(jsonObject);
                            holder.click.setVisibility(View.VISIBLE);
                            sub.setSubtitle(String.valueOf(participantsList.size() + " Selected"));
                            Log.d("addedList", "" + participantsList);
                            // notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });
*/