package com.zybertron.zoechat.baseUtils;

import org.json.JSONObject;

/**
 * Created by KrishnaDev on 1/10/17.
 */
public interface OnBottomReached {
    void onBottomReached();
}
